"""„Najważniejszym powodem, dla którego ludzie nie osiągają tego, co chcą,
 jest to, że sami nie wiedzą, czego chcą.” - Harv Eker"""
import csv
import sys


class Manager:
    def __init__(self):
        self.actions = {}
        self.history = []
        self.warehouse = {}
        self.sum = 0

    def assign(self, name):
        def decorate(cb):
            self.actions[name] = cb
        return decorate

    def execute(self, name, *args):
        if name not in self.actions:
            print("Action not defined")
        else:
            self.actions[name](self, *args)

    def open_file(self, filename):
        with open(filename, 'r') as f:
            reader = csv.reader(f)
            for line in reader:
                for element in line:
                    self.history.append(element.strip())
            return self.history

    def sum_account(self, history):
        i = 0
        while i < len(history):
            if history[i] == 'saldo':
                self.sum += int(history[i + 1])
            elif history[i] == 'zakup':
                self.sum -= int(history[i + 2]) * int(
                    history[i + 3])
            elif history[i] == 'sprzedaz':
                self.sum +=\
                    int(history[i + 2]) * int(history[i + 3])
            elif history[i] == 'stop':
                break
            i += 1
        return self.sum

    def show_warehouse(self, history):
        i = 0
        while i < len(history):
            if history[i] == 'zakup':
                product = history[i + 1]
                quantity = int(history[i + 3])
                if self.warehouse.get(product):
                    self.warehouse[product] += quantity
                else:
                    self.warehouse[product] = quantity
            elif history[i] == 'sprzedaz':
                product = history[i + 1]
                quantity = int(history[i + 3])
                if self.warehouse.get(product) and self.warehouse.get(product) \
                        >= quantity:
                    self.warehouse[product] -= quantity
            i += 1
        return self.warehouse

    def is_in_warehouse(self, questions):
        for element in questions:
            if element in self.warehouse:
                print(element)
                print(self.warehouse[element])
            else:
                print(element)
                print('0')

    def overview(self, _from, _to):
        if int(_from) < 0 or int(_to) > len(self.history):
            print('Blad! podano zle argumenty')
        else:
            _base = self.history[int(_from):int(_to)]
            for v in _base:
                print(v)

    @staticmethod
    def is_error(x_up, y_down, print_false):
        if x_up > y_down:
            print(print_false)
            return False
        else:
            return True

    @staticmethod
    def save_file(filename, file):
        with open(filename, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(file)


manager = Manager()


@manager.assign("saldo")
def saldo(manager):
    manager.history = manager.open_file(sys.argv[1])
    manager.history.remove('stop')
    manager.history.append('saldo')
    for element in sys.argv[2:]:
        manager.history.append(element)
    manager.history.append('stop')
    print(manager.history)
    manager.save_file(sys.argv[1], manager.history)


@manager.assign("zakup")
def zakup(manager):
    price = sys.argv[3]
    quantity = sys.argv[4]
    price_purchase = int(price) * int(quantity)
    manager.history = manager.open_file(sys.argv[1])

    if manager.is_error(price_purchase, manager.sum_account(manager.history), 'Za malo PLN'):
        manager.history.remove('stop')
        manager.history.append('zakup')
        for element in sys.argv[2:]:
            manager.history.append(element)
        manager.history.append('stop')
        manager.save_file(sys.argv[1], manager.history)


@manager.assign("magazyn")
def saldo(manager):
    questions = sys.argv[2:]
    manager.history = manager.open_file(sys.argv[1])
    print(manager.show_warehouse(manager.history))
    manager.is_in_warehouse(questions)


@manager.assign("sprzedaz")
def sprzedaz(manager):
    manager.history = manager.open_file(sys.argv[1])
    warehouse = manager.show_warehouse(manager.history)
    if manager.is_error(int(sys.argv[4]), int(warehouse[sys.argv[2]]), 'Za mało sztuk'):
        manager.history.remove('stop')
        manager.history.append('sprzedaz')
        for element in sys.argv[2:]:
            manager.history.append(element)
        manager.history.append('stop')
        manager.save_file(sys.argv[1], manager.history)


@manager.assign("konto")
def zakup(manager):
    manager.history = manager.open_file(sys.argv[1])
    print(manager.sum_account(manager.history))


@manager.assign("przeglad")
def saldo(manager):
    manager.history = manager.open_file(sys.argv[1])
    manager.overview(sys.argv[2], sys.argv[3])
